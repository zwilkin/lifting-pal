//
//  TrackerHomeVC.swift
//  Lifting Pal
//
//  Created by Zach on 3/15/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import UIKit

private let selectSegue = "selectWorkoutSegue"


class TrackerHomeVC: UIViewController {

// MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
// MARK: - Buttons
    // Track a Workout
    @IBAction func trackButton(_ sender: UIButton) {
        // Segue to "Select a Workout" to begin tracking
        performSegue(withIdentifier: selectSegue, sender: sender)
    }
    // Create New Routine
    @IBAction func createButton(_ sender: UIButton) {
        // Open "Create New Routine" tab
        tabBarController?.selectedIndex = 0
    }
    
    
// MARK: - Unwind From TrackerSelect // "Cancel"
    @IBAction func unwindFromSelect(segue: UIStoryboardSegue) {
    }
    
// MARK: - Unwind From TrackingVC // "Go Home" & "View Log"
    @IBAction func goHomeUnwind(segue: UIStoryboardSegue) {
        print("Successfully went home after saving a workout!")
    }
    
}



