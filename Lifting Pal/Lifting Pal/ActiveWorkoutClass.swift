//
//  ActiveWorkoutClass.swift
//  Lifting Pal
//
//  Created by Zach on 3/22/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import Foundation

public class ActiveWorkout {
    
// MARK: - Placeholder Variables
    var activeName: String = ""
    var activeCategory: String = ""
    var completedExercises: [Exercise] = []
    
    
// MARK: - Methods
    // Update Name & Category
    func inputDetails(name: String, category: String) {
        self.activeName = name
        self.activeCategory = category
    }
    
    // Update Number of Exercises Completed
    func addExerciseToCompleted(exercise: Exercise) {
        // Append the given Exercise to the completedExercises array
        self.completedExercises.append(exercise)
    }
    
    // Reset Instance
    func resetActive() {
        self.activeName = ""
        self.activeCategory = ""
        self.completedExercises = []
    }
    
    
    
// MARK: - Initiaizer(s)
    init() {}   // Blank initializer for placeholder purposes
    
    // New ActiveWorkout Initialization
    init(name: String, category: String) {
        self.activeName = name
        self.activeCategory = category
    }
    
}


// MARK: - Public ActiveWorkout Variable
// (used for detecting user input in tableView custom cells)
public var activeWorkoutSession: ActiveWorkout = ActiveWorkout()
