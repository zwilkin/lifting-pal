//
//  TrackerSelectVC.swift
//  Lifting Pal
//
//  Created by Zach on 3/15/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import UIKit

private let cellID = "workoutCell"
private let unwindSegue = "unwindFromSelect"
private let selectSegue = "workoutSelectedSegue"

class TrackerSelectVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

// MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Register Custom Cell Nib to the TableView
        workoutsTableView.register(UINib(nibName: "WorkoutCell", bundle: nil), forCellReuseIdentifier: cellID)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

// MARK: - Buttons & Outlets
    @IBAction func cancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var workoutsTableView: UITableView!
    
// MARK: - Placeholder Variables
    var selectedWorkout: Workout = Workout()
    
    
// MARK: - Prepare For Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == selectSegue {
            let destination = segue.destination as! DetailsVC
            // Pass selected workout to details view
            destination.chosenWorkout = selectedWorkout
        }
    }
    

    
// MARK: - TableView Delegate
    // Row Selection
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Set Selected Workout
        selectedWorkout = userWorkouts[indexPath.row]
        // Perform Segue
        performSegue(withIdentifier: selectSegue, sender: nil)
    }
    
    
    
// MARK: - TableView Data Source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userWorkouts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Use custom WorkoutCell
        let customCell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! WorkoutCell

        // Configure CustomCell Properties
        customCell.nameLabel?.text = userWorkouts[indexPath.row].name   // Workout Name
        customCell.exercisesLabel?.text = "\(userWorkouts[indexPath.row].exerciseArray.count)" // Number of Exercises as a String
        customCell.categoryLabel.text = userWorkouts[indexPath.row].category  // Category
        
        return customCell
    }
    
}
