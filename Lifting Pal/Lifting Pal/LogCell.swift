//
//  LogCell.swift
//  Lifting Pal
//
//  Created by Zach on 3/23/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import UIKit

class LogCell: UITableViewCell {

// MARK: - UI Objects
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    
    
// MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
