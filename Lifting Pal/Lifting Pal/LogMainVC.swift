//
//  LogMainVC.swift
//  Lifting Pal
//
//  Created by Zach on 3/23/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import UIKit
import CoreData

// Private constants
private let logCellIdentifier = "logCell"

class LogMainVC: UIViewController {

// MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Register custom LogCell xib to the tableView
        logTable.register(UINib(nibName: "LogCell", bundle: nil), forCellReuseIdentifier: logCellIdentifier)

        // Do any additional setup after loading the view.
        
        // TODO: TODO: CoreData Setup
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
// MARK: - Placeholder Variables
    

// MARK: - CoreData Implementation
    
    
// MARK: - Outlets
    @IBOutlet weak var logTable: UITableView!
    @IBOutlet weak var editButtonOutlet: UIBarButtonItem!
    @IBOutlet weak var deleteButtonOutlet: UIBarButtonItem!

    
// MARK: Buttons
    @IBAction func editButton(_ sender: Any) {
        // Check if currently editing
        if logTable.isEditing {
            // Editing
            editButtonOutlet.title = "Edit" // Change text to 'Edit'
            logTable.isEditing = false  // Enable table editing
        } else {
            // Not Editing
            editButtonOutlet.title = "Done" // Change text to 'Done'\
            logTable.isEditing = true
        }
    }
    
    
    
    
// MARK: - Functions & Methods
    // Obtain a string from given time elemtns
    func stringFromTimeTuple(tuple: (minutes: Int, seconds: Int, milliseconds: Int)) -> String {
        return String(format: "%02d:%02d:%02d", tuple.minutes, tuple.seconds, tuple.milliseconds)
    }
    
}




// MARK: - TableView Extension
extension LogMainVC: UITableViewDelegate, UITableViewDataSource {
    
// MARK: Cell Editing
    // Check if editing should be allowed
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if logTable.isEditing {
            return true // Editing
        } else {
            return false // Not Editing
        }
    }
    
    // Set editing style
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    
    // Editing Support
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        // As the user to confirm deletion
        let deleteAlert = UIAlertController(title: "Delete Forever?", message: "Are you sure you wish to delete this workout log?\nThere will be no way of retrieving this information once it is gone.", preferredStyle: .actionSheet)
        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        deleteAlert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
            // TODO: Handling deletion here
            savedUserWorkouts.remove(at: indexPath.row) // Remove the desired log from savedUserWorkouts public array
            self.logTable.reloadData()   // Reload logTable to reflect changes
        }))
        // Present deleteAlert to the user
        self.present(deleteAlert, animated: true, completion: nil)
    }
    
    
    
// MARK: Cell Declaration
    // Detemine number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return savedUserWorkouts.count
    }
    
    // Set up cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let logCell = tableView.dequeueReusableCell(withIdentifier: logCellIdentifier) as! LogCell
        logCell.titleLabel.text = savedUserWorkouts[indexPath.row].savedTitle
        logCell.categoryLabel.text = savedUserWorkouts[indexPath.row].workoutCompleted.category
        logCell.timeLabel.text = stringFromTimeTuple(tuple: savedUserWorkouts[indexPath.row].savedTime)
        
        return logCell
    }
    
}
