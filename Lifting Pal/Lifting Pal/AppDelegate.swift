//
//  AppDelegate.swift
//  Lifting Pal
//
//  Created by Zach on 3/9/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        // Set starting selected tab to Tracker (tab 2)
        let tabBarController = self.window?.rootViewController as! UITabBarController
        tabBarController.selectedIndex = 1
        
        // Load saved entities from CoreData
        //loadFromCoreData()
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    
    
// MARK: - CoreData Stack
    var applicationDocumentsDirectory: URL {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.fullsail.mdf2.CoreDataIntro" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count - 1]
    }
    
// MARK: Model
    var objectModel: NSManagedObjectModel {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "SavedData", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }
    
// MARK: Coordinator
    var persistentStoreCoordinator: NSPersistentStoreCoordinator {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.objectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        let failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // TODO: TODO: Error Handling
            // Replace this with code to handle the error appropriately.
            
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        return coordinator
    }
    
// MARK: Context
    var context: NSManagedObjectContext {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator   // Set context's coordinator
        return managedObjectContext
    }
    
// MARK: - CoreData Saving Support
    func saveContext() {
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Error handling implementation
                let saveError = error as NSError
                NSLog("Error Code \(saveError.code) - Save Failed For Reason:", saveError.localizedFailureReason!)
                abort()
            }
        }
    }
    
// MARK: Load from CoreData
    public func loadFromCoreData() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "SaveEntity")
        do {
            // Successfully fetched results
            print("Successfully fetches results!")
            if let fetchResults: [NSManagedObject] = try context.fetch(fetchRequest) as? [NSManagedObject] {
                
                // Check if there are any entries to be loaded
                if fetchResults.isEmpty {
                    // Results are empty - nothing to load
                    print("Fetched Results Empty!")
                } else {
                    // Loop through results
                    for result in fetchResults {
                        
                        print("\(result.value(forKey: "title")) has been loaded.")
                    }
                }
                
            }
        } catch {
            // Handle error on fetching request
            let fetchError = error as NSError
            NSLog("Error Code \(fetchError.code) - Save Failed For Reason:", fetchError.localizedFailureReason!)
            abort()
        }
        
    }
}



