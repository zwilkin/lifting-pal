//
//  ExerciseClass.swift
//  Lifting Pal
//
//  Created by Zach on 3/9/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import Foundation

public class Exercise {
    
// MARK: - Muscles Enum
    public enum MusclesEnum: String {
    // Arms
        case biceps = "Biceps"
        case triceps = "Triceps"
        case forearms = "Forearms"
    // Back
        case lats = "Latissimus Dorsi"
        case scapulae = "Scapulae"
        case rhomboid = "Rhomboids"
        case trapezius = "Trapezius"
        case lowerBack = "Lower Back"
    // Chest
        case pecsMajor = "Pectoralis Major"
        case pecsMinor = "Pectoralis Minor"
    // Core
        case abs = "Exterior Abdominus"
        case obliques = "Obliques"
    // Legs
        case hamstrings = "Hamstrings"
        case quads = "Quadriceps"
        case calves = "Calves"
        case abductors = "Groin"
    // Shoulders
        case deltFront = "Anterior Deltoid"
        case deltRear = "Posterior Deltoid"
        case deltLateral = "Lateral Deltoid"
        case rotatorCuff = "Rotator Cuff"
    }
    
    
// MARK: - Stored Properties
    var name: String
    var musclesUsed: [MusclesEnum] = []
    var description: String
    
    
    
// MARK: - Initializer(s)
    // Main Initializer
    init(name: String, musclesUsed: [MusclesEnum], description: String) {
        self.name = name
        self.musclesUsed = musclesUsed
        
        if description.isEmpty {
            self.description = "There is no desciption for this exercise."
        } else {
            self.description = description
        }
        
        
    }
    
    // Empty Initializer (for use with a placehodler to initialize empty instance)
    init() {
        self.name = ""
        self.musclesUsed = []
        self.description = ""
    }
    
}






// MARK: - Public exerciseList
private let muscle = Exercise.MusclesEnum.self  // shorten destination to be used in new Exercise() instance(s)
// Public list of exercises to be used when creating new workout routines
public let exerciseList: [Exercise] = [
        // Arm Exercises
    Exercise(name: "Bicep Curl", musclesUsed: [muscle.biceps, muscle.forearms], description: "curl your bicep"),
    Exercise(name: "Tricep Push-down", musclesUsed: [muscle.triceps], description: "push down triceps"),
    Exercise(name: "Seated Preacher Curl", musclesUsed: [muscle.biceps], description: "sit down and preach"),
    Exercise(name: "Skullcrushers", musclesUsed: [muscle.triceps, muscle.forearms], description: "crush your skull"),
        // Back Exercises
    Exercise(name: "Lat Pull-Down", musclesUsed: [muscle.lats, muscle.trapezius, muscle.scapulae, muscle.deltRear], description: "pull-down with your lats"),
    Exercise(name: "Bent-Over Rows", musclesUsed: [muscle.lowerBack, muscle.deltRear, muscle.scapulae], description: "bend over and row"),
    Exercise(name: "Pull-Ups", musclesUsed: [muscle.lats, muscle.trapezius, muscle.scapulae, muscle.rhomboid], description: "pull yourself up"),
        // Chest Exercises
    Exercise(name: "Flat Bench Press", musclesUsed: [muscle.pecsMajor, muscle.pecsMinor, muscle.rotatorCuff, muscle.deltFront], description: "bench press on a flat bench"),
    Exercise(name: "Incline Bench Press", musclesUsed: [muscle.pecsMajor, muscle.rotatorCuff, muscle.deltFront, muscle.deltLateral], description: "bench press on an incline bench"),
    Exercise(name: "Decline Bench Press", musclesUsed: [muscle.pecsMinor, muscle.rotatorCuff, muscle.deltFront, muscle.deltLateral], description: "bench press on a decline bench"),
        // Core Exercises
    Exercise(name: "Abdominal Crunch", musclesUsed: [muscle.abs], description: "crunch your abs - not sit up!"),
    Exercise(name: "Oblique Twists", musclesUsed: [muscle.obliques, muscle.abs], description: "twist your obliques"),
    Exercise(name: "Hanging Leg-Ups", musclesUsed: [muscle.abs, muscle.lowerBack], description: "hang and bring your legs up"),
        // Leg Exercises
    Exercise(name: "Barbell Squats", musclesUsed: [muscle.quads, muscle.hamstrings, muscle.calves, muscle.abductors], description: "with a barbell, squat down and back up"),
    Exercise(name: "Stiff-Leg Deadlift", musclesUsed: [muscle.hamstrings, muscle.lowerBack, muscle.scapulae, muscle.abs], description: "deadlift with a stiff leg"),
    Exercise(name: "Standing Calf Raise", musclesUsed: [muscle.calves, muscle.lowerBack, muscle.scapulae], description: "stand up on to your toes then back down"),
        // Shoulder Exercises
    Exercise(name: "Military Press", musclesUsed: [muscle.deltFront, muscle.scapulae], description: "press a barbell above your head"),
    Exercise(name: "Side Lateral Raise", musclesUsed: [muscle.deltLateral, muscle.rhomboid], description: "raise a dumbbell to your side"),
    Exercise(name: "Upright Rows", musclesUsed: [muscle.deltRear, muscle.trapezius, muscle.rhomboid, muscle.scapulae], description: "row a barbell upright")
    
]
