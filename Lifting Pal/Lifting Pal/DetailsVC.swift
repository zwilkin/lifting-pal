//
//  DetailsVC.swift
//  Lifting Pal
//
//  Created by Zach on 3/19/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import UIKit

// Private string constants
private let exerciseCell = "exerciseCell"   // Custom cell identifier
private let startWorkoutSegue = "startWorkoutSegue"  // Start Workout Segue identifier

class DetailsVC: UIViewController {

// MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Update labels to display information provided from passed chosenWorkout
        nameLabel.text = chosenWorkout.name
        categoryLabel.text = chosenWorkout.category
        setsLabel.text = String(chosenWorkout.setsPerExercise)
        
        // Loop through muscles in workout and add to single text array
        musclesLabel.text = ""  // Remove current placeholder text
        for muscle in chosenWorkout.musclesInWorkout {
            // Check for the last muscle to be added so that there is no comma added afterwards
            if chosenWorkout.musclesInWorkout[chosenWorkout.musclesInWorkout.count-1] == muscle {
                // This is the last muscle to be added - do not add a comma
                musclesLabel.text?.append(muscle)
            } else {
                // This is not the last muscle to be added - add a comma and a space
                musclesLabel.text?.append("\(muscle), ")
            }
        }   // End for muscle loop
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        debugPrint("!@!@! MEMORY WARNING !@!@!")
    }

// MARK: - Placeholder Variables
    var chosenWorkout: Workout = Workout()
    
    
    
    
// MARK: - Outlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var setsLabel: UILabel!
    @IBOutlet weak var musclesLabel: UILabel!
    @IBOutlet weak var exercisesTable: UITableView!
// MARK: Buttons
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func startButton(_ sender: Any) {
        // Display an alert to the user confirming that they are ready to begin the selected workout routine.
        let alert = UIAlertController(title: "Are you ready?", message: "Are you ready to start your workout?\nIf so, click Start!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Start", style: .default, handler: { (nil) in
            // Handle "Start" action here
            self.performSegue(withIdentifier: startWorkoutSegue, sender: sender)
        }))
        alert.addAction(UIAlertAction(title: "Wait", style: .cancel, handler: { (nil) in
            // Handle "Wait" action here - Do Nothing?
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
// MARK: - PrepareForSegue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Check for segue identifier
        if segue.identifier == startWorkoutSegue {
            let destination = segue.destination as! TrackingVC  // Set a destination
            destination.activeWorkout = chosenWorkout   // Pass user chosenWorkout to the TrackingVC
        } else { return }
    }
    
}   // - End DetailsVC


// MARK: - TableView Delegate & DataSource
extension DetailsVC: UITableViewDataSource, UITableViewDelegate {
    // Determine Number of Rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chosenWorkout.exerciseArray.count
    }
    
    // Cell For Row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: exerciseCell)
        cell.textLabel?.text = chosenWorkout.exerciseArray[indexPath.row].name
        return cell
    }
}
