//
//  SavedWorkoutClass.swift
//  Lifting Pal
//
//  Created by Zach on 3/22/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import Foundation

public class SavedWorkout {
    
// MARK: - Placeholder Variables
    var workoutCompleted: Workout = Workout()    // Placeholder for Workout to be initialized
    var exercisesCompleted: [Exercise] = [] // Exercises that were completed
    
    var savedTitle: String  // Title for the SavedWorkout instance
    var savedTime: (minutes: Int, seconds: Int, milliseconds: Int) = (0, 0, 0) // Tuple of hours/mins/secs from the elapsed workout time
    var savedDate: Date = Date()    // Date the SavedWorkout instance was saved
    
    // Convert the muscles in the workout to a single string - computed property
    var musclesString: String = "" {
        didSet {
            // Loop through muscles in workout and add to a single text variable
            for muscle in workoutCompleted.musclesInWorkout {
                // Check for the last muscle to be added to that there is no comma added afterwards
                if workoutCompleted.musclesInWorkout[workoutCompleted.musclesInWorkout.count-1] == muscle {
                    // This is the last muscle to be added - do not add a comma
                    musclesString.append(muscle)
                } else {
                    // This is not the last muscle to be added - add a comma and a space
                    musclesString.append("\(muscle), ")
                }
            }   // End for muscle loop
        }   // End didSet
    }
    
    
    
// MARK: - Functions & Methods
    // Duplicate string remover for musclesInWorkout
    func removeDuplicates(array: [String]) -> [String] {
        var encountered = Set<String>()
        var results: [String] = []
        
        for value in array {
            if encountered.contains(value) {
                // Do nothing - Duplicate found
            } else {
                // Not duplicate - add to the new array
                encountered.insert(value)   // Add value to the set
                results.append(value)   //  Append to results array
            }
        }
        return results
    }
    
    
    
// MARK: - Initializer(s)
    init(title: String, time: (minutes: Int, seconds: Int, milliseconds: Int), workout: Workout, exercisesCompleted: [Exercise]) {
        self.savedTitle = title
        self.savedTime = time
        self.workoutCompleted = workout
        self.exercisesCompleted = exercisesCompleted
        self.savedDate = Date.init()    // Initialize a new Date of the current date when the workout is saved
    }
}



// MARK: - Public SavedWorkout
// Public list of user's saved workouts to be used when displaying/editing workout history(log)
public var savedUserWorkouts: [SavedWorkout] = [
    SavedWorkout(title: "Awesome bench day!", time: (23, 47, 18), workout: userWorkouts[0], exercisesCompleted: [exerciseList[1], exerciseList[0]])
]
