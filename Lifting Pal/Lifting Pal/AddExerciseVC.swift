//
//  AddExerciseVC.swift
//  Lifting Pal
//
//  Created by Zach on 3/12/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import UIKit

class AddExerciseVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
// MARK: - Placeholder Variables
    // Unwind Segue (string)
    let unwindSegue: String = "unwindFromAdd"
    // Chosen Exercise (blank instance - temp)
    var selectedExercise: Exercise = Exercise()

    
// MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
// MARK: - Outlets & Buttons
    // Cancel Button
    @IBAction func cancelButton(_ sender: UIBarButtonItem) {
        print("Add exercise canceled - dismissing the ViewController")
        dismiss(animated: true, completion: nil)
    }
    // Search Bar Outlet
    @IBOutlet weak var searchBar: UISearchBar!      // SearchBar outlet
    @IBOutlet weak var exercisesTable: UITableView! // TableView outlet for reloading data
    
    
    
// MARK: - SearchBar Delegate
    var searchActive: Bool = false  // variable to track active/passive searchBar
    // Determine whether searchActive should be TRUE or FALSE
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) { searchActive = true }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) { searchActive = false }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) { searchActive = false }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) { searchActive = false }
    
    
    var filteredData: [Exercise] = [] // Temporary variable to hold filterd data
    // Detect text change in SearchBar - reloads table data dynamically
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filteredData = exerciseList.filter({ (Exercise) -> Bool in
            let temp: NSString = Exercise.name as NSString
            let range = temp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
        
        // Check if there are any matches to the search text
        if filteredData.count == 0 {
            searchActive = false
        } else {
            searchActive = true
        }
        self.exercisesTable.reloadData()    // Reload the TableView to display results as the user types
    }
    
    
    
    
    
// MARK: - TableView Delegate
    // Detemine Number of Rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchActive {
            return filteredData.count
        } else {
            return exerciseList.count
        }
    }
    // Create cell(s)
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "exerciseCell", for: indexPath)
        
        if searchActive {
            cell.textLabel?.text = filteredData[indexPath.row].name
        } else {
            cell.textLabel?.text = exerciseList[indexPath.row].name
        }
        return cell
    }
    
    
// MARK: Row Selected
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Check if currently filtering results
        if searchActive {
            selectedExercise = filteredData[indexPath.row]
        } else {
            selectedExercise = exerciseList[indexPath.row]
        }
        
        // Perform unwind segue
        self.performSegue(withIdentifier: unwindSegue, sender: self)
    }
    
    
// MARK: - PrepareForSegue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! CreateNewVC
        
        destination.chosenExercises.append(selectedExercise)
        print("Adding \(selectedExercise.name) to the list!")
    }
    
    
}
