//
//  WorkoutCell.swift
//  Lifting Pal
//
//  Created by Zach on 3/15/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import UIKit

class WorkoutCell: UITableViewCell {

// MARK: - UI Objects
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var exercisesLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    
    
// MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
