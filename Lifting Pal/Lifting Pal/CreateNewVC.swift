//
//  FirstViewController.swift
//  Lifting Pal
//
//  Created by Zach on 3/9/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import UIKit

// Private string constant for segue identifier
private let selectWorkoutSegue = "selectWorkoutSegue"

class CreateNewVC: UIViewController, UITabBarDelegate {
// MARK: - Placeholder Variables
    var chosenExercises: [Exercise] = []
    var chosenSets: Int = 3
    var chosenCategory: String = ""
    
    // Segue Variables
    let addExerciseSegue: String = "addExerciseSegue"
    
    // Category & Sets/Exercise 
    let categoriesValues: [String] = ["Choose...", "Arms", "Back", "Chest", "Core", "Legs", "Shoulders"]
    let setsValues: [Int] = [1, 2, 3, 4, 5, 6, 7, 8] // sets/exercise values
    
    
    
// MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Select 3 sets/exercise as default for setsPicker
        setsPicker.selectRow(2, inComponent: 0, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

// MARK: - Outlets / Buttons (& actions)
    // Outlets
    @IBOutlet weak var exercisesTable: UITableView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var categoryPicker: UIPickerView!
    @IBOutlet weak var setsPicker: UIPickerView!
    
    
    // Buttons
    @IBAction func addExerciseButton(_ sender: UIButton) {
        // Segue to AddExerciseVC
        performSegue(withIdentifier: addExerciseSegue, sender: sender)
    }
    
    @IBAction func resetButton(_ sender: Any) {
        // Confirm deletion before proceeding
        let alert = UIAlertController(title: "Are You Sure?", message: "Are you sure you wish to reset your current workout routine?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Hell Yeah!", style: .destructive, handler: { (UIAlertAction) in
            // Reset Input Fields
            self.resetFields()
        }))
        alert.addAction(UIAlertAction(title: "Actually...No", style: .default, handler: { (UIAlertAction) in
            // Cancel - DO NOTHING
            print("Action canceled by the user.")
        }))
        // Present alert controller to the user
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func createWorkout(_ sender: UIButton) {
        // Check for empty name
        if nameTextField.text!.isEmpty {
            // Name is empty - present alert
            let emptyNameAlert = createNewAlert(title: "Invalid Name", message: "You must specify a name for your workout routine.", style: .alert)
            self.present(emptyNameAlert, animated: true, completion: nil)
        } else {
            // Name is Valid
            // Check that there are at least 2 exercises in the list
            if exercisesTable.visibleCells.count < 2 {
                // Less than 2 exercises - present alert
                let exercisesAlert = createNewAlert(title: "Exercise List Too Short", message: "You must select at least 2 exercises before saving a new workout", style: .alert)
                self.present(exercisesAlert, animated: true, completion: nil)
            } else {
                // Name & Exercises are Valid
                if categoryPicker.selectedRow(inComponent: 0) == 0 {
                    // No Category chosen - present alert
                    let categoryAlert = createNewAlert(title: "Select a Category", message: "You must select a category before saving a new workout", style: .alert)
                    self.present(categoryAlert, animated: true, completion: nil)
                } else {
                    // Name, Exercise, & Category are Valid
                // CREATE WORKOUT HERE!!
                    let newWorkout: Workout = Workout(name: nameTextField.text!, category: chosenCategory, sets: chosenSets, exercises: chosenExercises)
                    
                    // TODO: TODO: Save Workout to CoreData
                    userWorkouts.append(newWorkout)
                    
                    // Perform Segue to SelectWorkoutVC
                    performSegue(withIdentifier: selectWorkoutSegue, sender: sender)
                    
                    // Print new number of workouts
                    print("The user has \(userWorkouts.count) saved workouts.")
                }
            }
        }
    }   // - End createWorkout()
    
    
// MARK: - Dynamic Functions
    // Create Alert
    // Dynamically creates an instance of a UIAlertController based upon passed variabled.
    // * NOTE * this does not create dynamic button(s) - option will be simple "Got It!"
    func createNewAlert(title: String, message: String, style: UIAlertControllerStyle) -> UIAlertController {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        alertController.addAction(UIAlertAction(title: "Got It!", style: .default, handler: { (UIAlertAction) in
        }))
        return alertController
    }
    // Reset Input Fields
    func resetFields() {
        self.nameTextField.text = ""    // Name
        self.chosenExercises.removeAll()    // Chosen Exercises
        self.categoryPicker.selectRow(0, inComponent: 0, animated: false)    // Category Picker
        self.chosenCategory = ""    // Chosen Category
        self.setsPicker.selectRow(2, inComponent: 0, animated: false)    // Sets Picker
        self.chosenSets = 3 // Sets/Exercise
        
        self.exercisesTable.reloadData()    // Reload Table (to update)
    }
    
    
    
// MARK: - Unwind Segue
    @IBAction func unwindFromAdd(segue: UIStoryboardSegue) {
        // Reload table to show new data
        exercisesTable.reloadData()
    }
    
    
// MARK: - PrepareForSegue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Check which segue is being performed
        if segue.identifier == selectWorkoutSegue {
            
            // Reset Input Variables
            resetFields()
            
            // Change TabBar Selected Index (to Tracker)
            self.tabBarController?.selectedIndex = 1
        }
    }
    
}   // - End CreateNewVC



// MARK: -  TableView Methods Extension
extension CreateNewVC: UITableViewDelegate, UITableViewDataSource {
    // Determine Number of Rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chosenExercises.count
    }
    // Create Cell(s) (recycle)
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = chosenExercises[indexPath.row].name
        return cell
    }
    
    
    // Enables 'Swipe to Delete' + handles data deletion
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Handle data deletion
            print("DELETING \(chosenExercises[indexPath.row].name) from list!")
            self.chosenExercises.remove(at: indexPath.row)
            // Reload table to update
            self.exercisesTable.reloadData()
        }
    }
}   // - End TableView Extension



// MARK: - PickerView Methods Extension
extension CreateNewVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int { return 1 }
    
    // Determine Number of Rows
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case categoryPicker:
            return categoriesValues.count
        case setsPicker:
            return setsValues.count
        default:
            return 0
        }
    }
    // Title For Row
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == categoryPicker {
            return categoriesValues[row]
        } else {
            if row == 0 {
                return "1 set"
            } else {
                return "\(setsValues[row]) sets"
            }
        }
    }
    // Row Selected
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == categoryPicker {
            self.chosenCategory = categoriesValues[row]
        } else {
            self.chosenSets = setsValues[row]
        }
    }
    
    
}   // - End PickerView Extension

