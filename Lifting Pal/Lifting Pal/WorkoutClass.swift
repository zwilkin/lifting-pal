//
//  WorkoutClass.swift
//  Lifting Pal
//
//  Created by Zach on 3/9/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import Foundation                                      

public class Workout {
    
// MARK: - Stored Properties
    var name: String
    var category: String
    let setsPerExercise: Int
    let exerciseArray: [Exercise]
    
    // Calculated from API
    var musclesInWorkout: [String] = []
    
    
    
// MARK: - Delegate Methods
    // Change the Name of the workout routine.
    func editName(newName: String) { self.name = newName }
    // Change the Category of the workout routine
    func editCategory(newCategory: String) { self.category = newCategory }
    

    
    
// MARK: - Duplicate String Remover
    func removeDuplicates(array: [String]) -> [String] {
        var encountered = Set<String>()
        var results: [String] = []
        
        for value in array {
            if encountered.contains(value) {
                // Do nothing - Duplicate found
            } else {
                // Not duplicate - add to the new array
                encountered.insert(value)   // Add value to the set
                results.append(value)   //  Append to results array
            }
        }
        return results
    }
    
    
    
// MARK: - Initializer(s)
    // Creating a new workout routine - This is the first initializer that a new class will use.
    init(name: String, category: String, sets: Int, exercises: [Exercise]) {
        self.name = name
        self.category = category
        self.setsPerExercise = sets
        self.exerciseArray = exercises
        
        // Add all muscles used from the exercises of a workout to a single array
        for e in exercises {
            // Array of Muscles
            let muscles: [Exercise.MusclesEnum] = e.musclesUsed
            
            for m in muscles {
                // Add muscle to the array
                musclesInWorkout.append(m.rawValue)
            }
        }
        // Filter out duplicates in the array of muscles
        musclesInWorkout = removeDuplicates(array: musclesInWorkout)
    }
    
    // Empty Initizer (for use with a placeholder to initialize empty instance)
    init () {
        self.name = ""
        self.category = ""
        self.setsPerExercise = 0
        self.exerciseArray = []
    }
    
}





// MARK: - Public userWorkouts
// Public list of user created workouts to be used when tracking/displaying user workouts
public var userWorkouts: [Workout] = [
    // Hard-coded workout for testing purposes
    // TODO: *Remove Hardcoded Workout Before Deployment*
    Workout(name: "Test Workout", category: "Arms", sets: 3, exercises: [exerciseList[1], exerciseList[2], exerciseList[3], exerciseList[4], exerciseList[5]])
]
