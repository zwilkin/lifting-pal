//
//  ExerciseCell.swift
//  Lifting Pal
//
//  Created by Zach on 3/21/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import UIKit

class ExerciseCell: UITableViewCell {
    
// MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
// MARK: - Placeholder Variables
    var thisExercise: Exercise = Exercise()
    var isCompleted: Bool = false
    
// MARK: - Outlets
    @IBOutlet weak var exerciseName: UILabel!
    @IBOutlet weak var muscleList: UILabel!
    @IBOutlet weak var checkSwitch: UISwitch!   // Switch Outlet
    
    
// MARK: Actions
    @IBAction func switchAction(_ sender: UISwitch) {
        // Save On/Off state (On = completed)
        if sender.isOn {
            isCompleted = true
            
            // Disable/Gray out cell upon exercise completion (user input)
            sender.isEnabled = false
            self.backgroundColor = UIColor.darkGray.withAlphaComponent(0.25)
            
        } else {
            isCompleted = false
        }
    }
    
    
    
}
