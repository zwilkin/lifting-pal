//
//  TrackingVC.swift
//  Lifting Pal
//
//  Created by Zach on 3/21/17.
//  Copyright © 2017 zApps. All rights reserved.
//

import UIKit
import CoreData

// Private constants
private let exerciseCell = "exerciseCell"
private let goHomeUnwind = "goHomeUnwind"
private let viewLogUnwind = "viewLogUnwind"
private let saveEntityText = "SaveEntity"

class TrackingVC: UIViewController {
    
// MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initialize Workout Details
        nameLabel.text = activeWorkout.name // Workout Name
        categoryLabel.text = activeWorkout.category // Category
        setsLabel.text = String(activeWorkout.setsPerExercise)
        exerciseTotalLabel.text = "(\(activeWorkout.exerciseArray.count) total)"
        
        // Loop through muscles in workout and add to single text array
        musclesLabel.text = ""  // Remove current placeholder text
        for muscle in activeWorkout.musclesInWorkout {
            // Check for the last muscle to be added so that there is no comma added afterwards
            if activeWorkout.musclesInWorkout[activeWorkout.musclesInWorkout.count-1] == muscle {
                // This is the last muscle to be added - do not add a comma
                musclesLabel.text?.append(muscle)
            } else {
                // This is not the last muscle to be added - add a comma and a space
                musclesLabel.text?.append("\(muscle), ")
            }
        }   // End for muscle loop
        exerciseCompleteLabel.text = "\(activeWorkoutSession.completedExercises.count) of \(activeWorkout.exerciseArray.count) exercises completed."
        // Register Custom ExerciseCell xib
        exercisesTable.register(UINib(nibName: "ExerciseCell", bundle: nil), forCellReuseIdentifier: exerciseCell)
        
        // Make sure that the Pause button is enabled, and the Play button is disabled
        pauseButtonOutlet.isEnabled = true  // Enable Pause
        playButtonOutlet.isEnabled = false  // Disable Play
        
        
        // MARK: Set up CoreData
        /* Make sure our Stored property ObjectContext is same as pre-existing in App Delegate */
        context = appDelegate.context
        
        // Fill out Entity Description
        entityDescription = NSEntityDescription.entity(forEntityName: saveEntityText, in: context)
        
        // Use description to make numTaps a "NumTaps" entity
        saveEntity = NSManagedObject(entity: entityDescription, insertInto: context)
        
}
    override func viewDidAppear(_ animated: Bool) {
        // Start the timer once the workout begins
        startTimer()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(_ animated: Bool) {
        resetActiveTracker()    // Reset Active Tracker details when the view disapears
    }
    
// MARK: - Placeholder variables
    var activeWorkout: Workout = Workout()
    var exercisesCompleted: [Exercise] = []

    
// MARK: - CoreData Implementation
    public let appDelegate = UIApplication.shared.delegate as! AppDelegate // Local reference to AppDelegate
    public var context: NSManagedObjectContext!   // Our 'notepad' -- data 'middleman' between code and hard drive
    public var entityDescription: NSEntityDescription!  // Helps build entity by describing a specific Entity Type
    public var saveEntity: NSManagedObject! // Represents entity 'SaveEntity' from .xcdatamodeld file
    
    // MARK: Save to CoreData
    func saveToCoreData(title: String, category: String, time: (minutes: Int, seconds: Int, milliseconds: Int)) {
        // Set Entity Valus
        saveEntity.setValue(title, forKey: "title")
        saveEntity.setValue(category, forKey: "category")
        saveEntity.setValue(time.minutes, forKey: "timeMinutes")
        saveEntity.setValue(time.seconds, forKey: "timeSeconds")
        saveEntity.setValue(time.milliseconds, forKey: "timeMilliseconds")
        
        // Invoke saveContext method from AppDelegate - saves the data
        appDelegate.saveContext()
        print("Entity saved to CoreData")
    }
    

// MARK: - Outlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var musclesLabel: UILabel!
    @IBOutlet weak var setsLabel: UILabel!
    
    @IBOutlet weak var exercisesTable: UITableView!
    @IBOutlet weak var timerLabel: UILabel!
    
    @IBOutlet weak var exerciseTotalLabel: UILabel!
    @IBOutlet weak var exerciseCompleteLabel: UILabel!
    
    @IBOutlet weak var pauseButtonOutlet: UIBarButtonItem!
    @IBOutlet weak var playButtonOutlet: UIBarButtonItem!
    
    
// MARK: - PrepareForSegue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Reset Tracking variables
        resetActiveTracker()
    }
    
    
    
// MARK: Buttons
    // Finish
    @IBAction func finishButton(_ sender: Any) {
        pauseTimer()    // Stop the timer
        pauseButtonOutlet.isEnabled = false // Disable "Pause" Button
        playButtonOutlet.isEnabled = true   // Enable "Play" Button
        print("Workout Finished In:", stringFromTimeTuple(tuple: timeTuple))    // Print total length of elapsedTime
        
        // Create alert asking user to Save/Discard the completed workout
        let saveAlert = UIAlertController(title: "Save This Workout?", message: "Would you like to save or discard this completed workout?", preferredStyle: .alert)
        
        
        // Save Button Action (on saveAlert)
        saveAlert.addAction(UIAlertAction(title: "Save", style: .default, handler: { (action) in
            
            // Prompt user to enter a title to be used for this SavedWorkout instance
            let titleAlert =  UIAlertController(title: "Enter a Title", message: "Enter a title for this saved workout", preferredStyle: .alert)
            titleAlert.addTextField(configurationHandler: { (textField) in
                textField.placeholder = "Enter a title..."  // Change new textField's placeholder text
            })
            // Save Button Action (on titleAlert)
            titleAlert.addAction(UIAlertAction(title: "Save", style: .default, handler: { (action) in
                let titleText = titleAlert.textFields![0]   // Force unwrapping because we know it exists
                
                if titleText.text!.isEmpty {
                    // Alert the user that there is not enough characters entered
                    let invalidTextAlert = UIAlertController(title: "No Text Entered", message: "Please enter title for this workout.", preferredStyle: .alert)
                    invalidTextAlert.addAction(UIAlertAction(title: "Enter Title", style: .default, handler: { (action) in
                        // Re-present titleAlert to the user once they click "Ok"
                        self.present(titleAlert, animated: true, completion: nil)
                    }))
                    self.present(invalidTextAlert, animated: true, completion: nil)
                    
                } else {
                    // Text entered
                    print("Saving new log entry with the title: \"\(titleText.text!)\"")
                    
                    
                    // Save workout when user clicks "Save" and valid text is entered
                    savedUserWorkouts.append(SavedWorkout(title: titleText.text!, time: self.timeTuple, workout: self.activeWorkout, exercisesCompleted: self.exercisesCompleted))
                    
                    
                // MARK: Save to CoreData
                    // Custom function to dynamically save information
                    self.saveToCoreData(title: titleText.text!, category: self.activeWorkout.category, time: self.timeTuple)
                    
                    
                    print("The user completed \(savedUserWorkouts.last!.exercisesCompleted.count) out of their \(savedUserWorkouts.last!.workoutCompleted.exerciseArray.count) exercises.")
                    
                    // Alert the user that the workout has been successfully saved
                    let savedAlert = UIAlertController(title: "Workout Saved!", message: "Your workout \"\(titleText.text!)\" has been successfully saved!", preferredStyle: .alert)
                    savedAlert.addAction(UIAlertAction(title: "Go Home", style: .default, handler: { (action) in
                        self.performSegue(withIdentifier: goHomeUnwind, sender: action)
                    }))
                    
                    // Present savedAlert to the user
                    self.present(savedAlert, animated: true, completion: nil)
                }
                
            })) // End "Save: Action
            
            // Cancel Button Action (on titleAlert)
            titleAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
            // Present titleAlert to the user
            self.present(titleAlert, animated: true, completion: { 
                // Dismiss the saveAlert once the titleAlert has finished presenting
                saveAlert.dismiss(animated: true, completion: nil)
            })
            
        })) // End Save (on saveAlert)
        
        
        // Discard Button Action
        saveAlert.addAction(UIAlertAction(title: "Discard", style: .cancel, handler: { (action) in
            //
        }))
        
        // Present saveAlert to the user
        self.present(saveAlert, animated: true, completion: nil)
        
    }   // End Finish
    
    // Pause
    @IBAction func pauseButton(_ sender: Any) {
        // Pause button pressed - disable and enable the play button
        pauseButtonOutlet.isEnabled = false // Disable Pause
        playButtonOutlet.isEnabled = true   // Enable Play
        pauseTimer()    // Pause Timer
        
        // Loop through cells in exerciseTable and disable the switches from being toggleable
        for cell in exercisesTable.visibleCells {
            let cell = cell as! ExerciseCell    // Set the cell to custom ExerciseCell
            cell.checkSwitch.isUserInteractionEnabled = false   // Disable UserInteraction
        }
    }
    
    // Play
    @IBAction func playButton(_ sender: Any) {
        // Play button pressed - disable and enable the pause button
        playButtonOutlet.isEnabled = false  // Disable Play
        pauseButtonOutlet.isEnabled = true  // Enable Pause
        startTimer()    // Start Timer
        
        // Loop through cells in exerciseTable and disable the switches from being toggleable
        for cell in exercisesTable.visibleCells {
            let cell = cell as! ExerciseCell    // Set the cell to custom ExerciseCell
            cell.checkSwitch.isUserInteractionEnabled = true    // Enable UserInteraction
        }
    }
    
    // Stop
    @IBAction func stopButton(_ sender: Any) {
        pauseTimer()    // Pause the timer while user makes a decision
        
        // Request user confirmation before stopping the workout - create & present an alert
        let stopAlert = UIAlertController(title: "Do You Wish to Quit?", message: "Are you sure you want to quit?\nYour progress will not be saved.", preferredStyle: .alert)
        stopAlert.addAction(UIAlertAction(title: "Continue", style: .default, handler: { (action) in
            // Continue the workout
            self.startTimer()    // Start the timer
        }))
        stopAlert.addAction(UIAlertAction(title: "Quit", style: .cancel, handler: { (action) in
            // Stop the workout
            self.resetTimer()    // Reset the timer
            self.dismiss(animated: true, completion: nil)   // Dismiss the view controller
        }))
        self.present(stopAlert, animated: true) { print("stopAlert has finished presenting.") } // Present stopAlert to the user
    }
    
    // Rest
    func resetActiveTracker() {
        exercisesCompleted.removeAll()  // Remove all elements from exercisesCompleted array
        activeWorkout = Workout()   // Reset the activeWorkout
        activeWorkoutSession = ActiveWorkout()  // Reset the activeWorkoutSession
        exercisesTable.reloadData() // Reload tableView
        
        // Reset UI Elements
        nameLabel.text = "Workout Name"
        categoryLabel.text = "Category Label"
        musclesLabel.text = "Muscle 1, Muscle 2, Mucsle 3, Muscle 4"
        exerciseTotalLabel.text = "(0 total)"
        setsLabel.text = "10"
        exerciseCompleteLabel.text = "0 of 10 exercises completed"
    }
    
    
// MARK: - UISwitch Target Selector
// Occurs when the switch changes states
    func switchChanged(sender: UISwitch) {
        let thisExercise: Exercise = activeWorkout.exerciseArray[sender.tag]   // Detect the specific exercise to be completed
        
        // Custom delegate method to add completed exercise to the list of activeWorkoutSessions completedExercises array
        activeWorkoutSession.addExerciseToCompleted(exercise: thisExercise)
        exercisesCompleted = activeWorkoutSession.completedExercises
        
        print("User has completed \(thisExercise.name). Adding to the completed list!")
        
        // Update "0 of 10 exercises completed"
        exerciseCompleteLabel.text = "\(activeWorkoutSession.completedExercises.count) of \(activeWorkout.exerciseArray.count) exercises completed"
    }
    
    
    
    
    
// MARK: - Timer Functionality
    var timerObject: Timer = Timer()  // Timer Object
    var elapsedTime: Double = 0 // Elapsed Time Variable
    var isPaused: Bool = false
    var timeTuple: (minutes: Int, seconds: Int, milliseconds: Int) = (0, 0, 0)
    
    // Start
    func startTimer() {
        timerObject = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(timeCounter), userInfo: nil, repeats: true)
        isPaused = false
    }
    // Pause
    func pauseTimer() {
        timerObject.invalidate()  // Pauses the timer
        isPaused = true
    }
    // Reset
    func resetTimer() {
        timerObject.invalidate()    // Pause the timer
        isPaused = true
        elapsedTime = 0 // Reset elapsedTime back to 0
    }
    
    // Time Counter (#selector method)
    // Triggered every time the timer hits it's scheduled interval (eg. 0.01 = 1 ms)
    func timeCounter() {
        elapsedTime += 1    // Incrememnt elapsedTime every time the timeCounter function is triggered (every 1ms)
        
        timerLabel.text = stringFromTimeInterval(interval: elapsedTime)
    }
    
    
    
    
    
// MARK: - Functions/Methods
    // Obtain time elements (hour/min/sec) from an elapsedTime interval
    func stringFromTimeInterval(interval: TimeInterval) -> String {
        let interval = Int(interval)
        let milliseconds = interval % 100
        let seconds = (interval / 60) % 60
        let minutes = (interval / 3600)
        
        timeTuple = (minutes, seconds, milliseconds)   // Update the timeTuple which is used for final results
        return String(format: "%02d:%02d:%02d", minutes, seconds, milliseconds)
    }
    
    // Obtain a string from given time elemtns
    func stringFromTimeTuple(tuple: (minutes: Int, seconds: Int, milliseconds: Int)) -> String {
        return String(format: "%02d:%02d:%02d", tuple.minutes, tuple.seconds, tuple.milliseconds)
    }
    
    
}   // End TrackingVC




// MARK: - TableView Extension
extension TrackingVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activeWorkout.exerciseArray.count
    }
    
    // Define Custom ExerciseCell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: exerciseCell, for: indexPath) as! ExerciseCell
        
        // Configure cell elements
        cell.thisExercise = activeWorkout.exerciseArray[indexPath.row]  // Pass needed exercise to the cell's custom class
        cell.exerciseName.text = activeWorkout.exerciseArray[indexPath.row].name    // Change custom cell properties
        cell.checkSwitch.tag = indexPath.row    // Set cell's switch's tag to specific row
        cell.checkSwitch.addTarget(self, action: #selector(switchChanged(sender:)), for: .touchUpInside)    // Add target action to the cell
        
        cell.muscleList.text = ""  // Remove current placeholder text
        // Loop through muscles in a specific workout and add them to the musclesList label
        for muscle in activeWorkout.exerciseArray[indexPath.row].musclesUsed {
            // Check for last muscle to be added so there is no comma afterwards
            if activeWorkout.exerciseArray[indexPath.row].musclesUsed[activeWorkout.exerciseArray[indexPath.row].musclesUsed.count-1] == muscle {
                // This is the last muscle to be added - do not add a comma
                cell.muscleList.text?.append(muscle.rawValue)
            } else {
                // This is not the last muscle to be added - add a comma and a space
                cell.muscleList.text?.append("\(muscle.rawValue), ")
            }
        }   // End for muscle loop
        return cell
    }
    
}   // End extension
