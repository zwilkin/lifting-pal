# README #

This repository is for the "Fitness Pal" iOS application - created by Zach Wilkin for Project & Portfolio IV.


### PLEASE NOTE ###
This application is developed solely for iPhones - not iPads. The reason this is not being developed for iPads
is because this application's intended use is during your workouts - tracking in realtime.

Please note that this application’s screen resolution was developed for iPhone 7S/7/6S/6 Portrait Mode.

That being said, how many times do you pull out a big iPad in the middle of the gym to log a set? 
- Zach Wilkin


###### INCOMPLETE FEATURES... ######
The only feature listed on my functional requirements that does not properly work is loading the information from CoreData. 
The application successfully saves the user data to CoreData, but does not retrieve.
